/*******************************************************************************
*
* Watchdog Timer: Test project to learn watchdog timer
*
*    Watchdog Timer:              copyright � 2015, Dmytro Bernyk.
*
*    THIS SOFTWARE IS PROVIDED BY THE DMITRY BERNYK AND CONTRIBUTORS "AS IS"
*    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
*    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
*    PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL DMITRY FRANK OR CONTRIBUTORS BE
*    LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
*    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
*    SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
*    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
*    CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
*    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
*    THE POSSIBILITY OF SUCH DAMAGE.
*
******************************************************************************/

#include "io430.h"
#include <stdint.h>
#include <intrinsics.h>

#include "timer.h"
#include "messages.h"

#define RED_ON      P1OUT |= BIT0
#define RED_OFF     P1OUT &= ~BIT0
#define GREEN_ON      P1OUT |= BIT6
#define GREEN_OFF     P1OUT &= ~BIT6

#define LED_ON_TIME     10
#define LED_OFF_TIME    10000

#define L_TIME_PRESS    1500        // 2000ms <= TIME < 10000ms
#define S_TIME_PRESS    50         // 100ms <= TIME < 2000ms
#define T_TIME_PRESS    10000       // More that 10s it is error state

#define BUTTON_SM_TIMER TIMER_1
#define LED_SM_TIMER TIMER_2

#define L_PRESS_MSG MESSAGE_1
#define S_PRESS_MSG MESSAGE_2

//User type defenition
typedef enum
{
    NOT_PRESS = (uint8_t)0, //Not press
    ERROR_RELEASE,          //Time out to release button error
    W_RELEASE               //Waiting pressed button is released
}button_state_t;

typedef enum
{
    LED_ON = (uint8_t)0,
    LED_OFF,
}led_state_t;


//Function prtotypes
void Button_SM_Init(void);
void Button_SM_Process(void);
void Led_SM_Init(void);
void Led_SM_Process(void);

//Global variabless
button_state_t button_state;
led_state_t led_state;

uint16_t led_on_time = 10;
uint16_t led_off_time = 10000;


void main( void )
{
    // Stop watchdog timer to prevent time out reset
    WDTCTL = WDTPW + WDTHOLD + WDTCNTCL;
    
    //Config clock system
    BCSCTL3 = LFXT1S1;
    BCSCTL2 = DIVS1;        //SMCLK source DCO/4
    DCOCTL = 0x00;
    DCOCTL = CALDCO_1MHZ;
    BCSCTL1 = CALBC1_1MHZ;
    
    //Config WDT in interval mode
//    WDTCTL = WDTPW + WDTTMSEL + WDTSSEL;
//    IE1 = WDTIE;  
    
    TACCR0 = 11;
    TACTL = TASSEL0 + MC0 + TAIE;
    
    //Config GPIO
    P1DIR = 0x41;     //Red and green led pin conf as output
    RED_OFF;
    GREEN_OFF;
    
    P1REN |= BIT3;    //Enable pulling resistor for 3 pin
    P1OUT |= BIT3;    //PullUP resistor
    
    
    __enable_interrupt();
    
    Timer_Init();
    Message_Init();
    Button_SM_Init();
    Led_SM_Init();
    
    while(1)
    {
        Button_SM_Process();
        Led_SM_Process();
        
        if(Message_Get(L_PRESS_MSG))
        {
            led_off_time = 200;
            led_on_time = 200;
        }
        if(Message_Get(S_PRESS_MSG))
        {
            led_off_time = 1000;
            led_on_time = 1000;
        }
        
        Message_Process();
        LPM3;
    }
}


#pragma vector=TIMER0_A1_VECTOR
__interrupt void Timer0_A1_ISR(void)
{
    Timer_count();
    TACTL &= ~TAIFG;
    LPM3_EXIT;
}


#pragma vector=WDT_VECTOR
__interrupt void WDT_ISR(void)
{
    P1OUT ^= 0x01;
}


inline void Button_SM_Init(void)
{
    button_state = NOT_PRESS;
    Timer_Reset(BUTTON_SM_TIMER);
}

void Button_SM_Process(void)
{
    switch (button_state)
    {
        case NOT_PRESS:
            if(!(P1IN & BIT3))
            {
                Timer_Start(BUTTON_SM_TIMER);
                button_state = W_RELEASE;
            }
            break;
        
        case W_RELEASE:
            if(P1IN & BIT3)
            {
                Timer_Stop(BUTTON_SM_TIMER);
                
                if(Timer_Get(BUTTON_SM_TIMER) >= L_TIME_PRESS)
                {
                    Message_Send(L_PRESS_MSG);
                }else if(Timer_Get(BUTTON_SM_TIMER) >= S_TIME_PRESS)
                {
                    Message_Send(S_PRESS_MSG);
                }else
                {
                    button_state = NOT_PRESS;
                }
                
                Timer_Reset(BUTTON_SM_TIMER);
            }
            
            if(Timer_Get(BUTTON_SM_TIMER) > T_TIME_PRESS) button_state = ERROR_RELEASE;
            break;
            
        case ERROR_RELEASE:                             // In this case whe only stop timer
             Timer_Stop(BUTTON_SM_TIMER);               // and wait while button will by released
             Timer_Reset(BUTTON_SM_TIMER);
             if(P1IN & BIT3) button_state = NOT_PRESS;
            break;
            
        default:
            break;
    }
}


inline void Led_SM_Init(void)
{
    led_state = LED_OFF;
    Timer_Reset(LED_SM_TIMER);
    Timer_Start(LED_SM_TIMER);
}

void Led_SM_Process(void)
{
    switch (led_state)
    {
        case LED_ON:
            if(Timer_Get(LED_SM_TIMER) >= led_on_time)
            {
                Timer_Reset(LED_SM_TIMER);
                led_state = LED_OFF;
                GREEN_OFF;
            }
            break;
            
        case LED_OFF:
            if(Timer_Get(LED_SM_TIMER) >= led_off_time)
            {
                Timer_Reset(LED_SM_TIMER);
                led_state = LED_ON;
                GREEN_ON;
            }
            break;
                
        default:
            break;
    }
}