/*******************************************************************************
*
*    messages.h: Header file of library provides functions transmit
*                and receive messages
*
*    Messages lib:              copyright � 2015, Dmytro Bernyk.
*
*    THIS SOFTWARE IS PROVIDED BY THE DMITRY BERNYK AND CONTRIBUTORS "AS IS"
*    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
*    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
*    PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL DMITRY FRANK OR CONTRIBUTORS BE
*    LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
*    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
*    SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
*    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
*    CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
*    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
*    THE POSSIBILITY OF SUCH DAMAGE.
*
******************************************************************************/

#ifndef __MESSAGES
#define __MESSAGES

#include "io430.h"
#include <stdint.h>
#include <intrinsics.h>

#define MAX_MESSAGES    6
#define MESSAGE_1   0
#define MESSAGE_2   1
#define MESSAGE_3   2
#define MESSAGE_4   3
#define MESSAGE_5   4
#define MESSAGE_6   5


extern volatile uint8_t msg[MAX_MESSAGES];

#define Message_Send(message_id)   msg[message_id] = 2

void Message_Init(void);
void Message_Process(void);
uint8_t Message_Get(uint8_t message_id);

#endif