/*******************************************************************************
*
*    messages.c: provides functions transmit and receive messages
*
*    Messages lib:              copyright � 2015, Dmytro Bernyk.
*
*    THIS SOFTWARE IS PROVIDED BY THE DMITRY BERNYK AND CONTRIBUTORS "AS IS"
*    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
*    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
*    PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL DMITRY FRANK OR CONTRIBUTORS BE
*    LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
*    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
*    SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
*    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
*    CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
*    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
*    THE POSSIBILITY OF SUCH DAMAGE.
*
******************************************************************************/

#include "messages.h"

volatile uint8_t msg[MAX_MESSAGES];

inline void Message_Init(void)
{
    uint8_t cnt;
    for(cnt = 0; cnt < MAX_MESSAGES; cnt++)
    {
        msg[cnt] = 0;
    }
}


inline void Message_Process(void)
{
    uint8_t cnt;
    for(cnt = 0; cnt < MAX_MESSAGES; cnt++)
    {
        if(msg[cnt] == 1) msg[cnt] = 0;
        if(msg[cnt] == 2) msg[cnt] = 1;
    }
}

uint8_t Message_Get(uint8_t message_id)
{
    uint8_t rt = 0;
    if(msg[message_id] == 1) rt = 1;
    return rt;
}