/*******************************************************************************
*
*    timer.h: Add program timer capability
*
*    Program timer:              copyright � 2015, Dmytro Bernyk.
*
*    THIS SOFTWARE IS PROVIDED BY THE DMITRY BERNYK AND CONTRIBUTORS "AS IS"
*    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
*    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
*    PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL DMITRY FRANK OR CONTRIBUTORS BE
*    LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
*    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
*    SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
*    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
*    CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
*    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
*    THE POSSIBILITY OF SUCH DAMAGE.
*
******************************************************************************/

#ifndef __TIMER
#define __TIMER

#include "io430.h"
#include <stdint.h>
#include <intrinsics.h>

typedef uint16_t counter_t;        //Program timer max value

typedef enum
{
    STOP = (uint8_t)0,
    RUN
}tstate_t;

typedef struct
{
    counter_t cnt;
    tstate_t state;
}timer_t;

#define TIMER_1     0
#define TIMER_2     1
#define TIMER_3     2
#define TIMER_4     3
#define TIMER_5     4
#define TIMER_6     5

#define MAX_TIMERS  6

extern volatile timer_t tmr[MAX_TIMERS];


#define Timer_Reset(timer)  tmr[timer].cnt = 0
#define Timer_Stop(timer)   tmr[timer].state = STOP
#define Timer_Start(timer)  tmr[timer].state = RUN
#define Timer_Get(timer)    tmr[timer].cnt

void Timer_count(void);
void Timer_Init(void);

#endif