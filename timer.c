/*******************************************************************************
*
*    timer.c: 
*
*    Programm Timer:              copyright � 2015, Dmytro Bernyk.
*
*    THIS SOFTWARE IS PROVIDED BY THE DMITRY BERNYK AND CONTRIBUTORS "AS IS"
*    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
*    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
*    PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL DMITRY FRANK OR CONTRIBUTORS BE
*    LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
*    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
*    SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
*    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
*    CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
*    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
*    THE POSSIBILITY OF SUCH DAMAGE.
*
******************************************************************************/

#include "timer.h"

volatile timer_t tmr[MAX_TIMERS];

void Timer_Init(void)
{
    uint8_t i;
    for(i = 0; i < MAX_TIMERS; i++)
    {
        tmr[i].cnt = 0;
        tmr[i].state = STOP;
    }
}

void Timer_count(void)
{
    uint8_t i;
    for(i = 0; i < MAX_TIMERS; i++)
    {
        if(tmr[i].state == RUN) tmr[i].cnt++;
    }
}